import React, { Component } from 'react';
import Message from '../Message/Message';

class Chat extends Component {
  state = {
    messages: [],
    messageInput: ''
  };

  messageListRef = React.createRef();

  componentDidUpdate(prevProps, prevState) {
    const prevMessages = prevState.messages;
    const {
      state: { messages },
      scrollToBottom
    } = this;

    if (messages.length > prevMessages.length) {
      scrollToBottom();
    }
  }

  changeInputMessage = e => {
    this.setState({ messageInput: e.target.value });
  };

  sendMessageOnEnter = e => {
    let { messageInput, messages } = this.state;
    if (e.key === 'Enter' && messageInput) {
      this.setState({
        messages: [...messages, { text: messageInput, time: Date.now() }],
        messageInput: ''
      });
    }
  };

  scrollToBottom = () => {
    const {
      messageListRef: { current }
    } = this;

    current.lastChild.scrollIntoView({
      behavior: 'smooth',
      block: 'end',
      inline: 'nearest'
    });
  };

  render() {
    const {
      state: { messageInput, messages },
      changeInputMessage,
      sendMessageOnEnter,
      messageListRef
    } = this;

    return (
      <div className="chat">
        <div className="window-messages" ref={messageListRef}>
          {messages.map(({ text, time }) => (
            <Message text={text} key={time} />
          ))}
        </div>

        <input
          className="input-message"
          onChange={changeInputMessage}
          value={messageInput}
          onKeyPress={sendMessageOnEnter}
        />
      </div>
    );
  }
}

export default Chat;
